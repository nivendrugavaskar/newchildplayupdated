//
//  KTALoadContentViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 18/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ChildsPlayJsonURL [NSURL URLWithString:@"http://dev1.businessprodemo.com/kta/php/downloadJSON.php"]



#import "KTALoadContentViewController.h"
#import "KTAViewController.h"
#import "KTAMapViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface KTALoadContentViewController () {
    NSMutableArray *content;
}

@end

@implementation KTALoadContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // load all content
    [[self navigationController] setNavigationBarHidden:YES];
    
    // get background depending on device screen size
//    UIImage *backgroundImage = [[UIImage alloc] init];
//    if ([UIScreen mainScreen].bounds.size.height > 480.0f) {
//        backgroundImage = [UIImage imageNamed:@"iphone5Background.jpg"];
//    } else {
//        backgroundImage = [UIImage imageNamed:@"iphoneBackground.jpg"];
//    }
// set the background image
// self.mainView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        /*
        backgroundImage=[UIImage imageNamed:@"iphone5Background.jpg"];
        self.mainView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
        UIImageView *imgview=[[UIImageView alloc] initWithFrame:CGRectMake(self.mainView.frame.origin.x, self.mainView.frame.origin.y, self.mainView.frame.size.width, self.mainView.frame.size.height)];
        imgview.image=backgroundImage;
        [self.mainView addSubview:imgview];
         */
        
    }
    
    [self performSelector:@selector(nextView) withObject:self afterDelay:2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)nextView {
    [self performSegueWithIdentifier:@"mainSegue" sender:self];
}

#pragma mark - Get favorites list from bundle
//- (void)checkAndGetFavoritesList {
//    BOOL success;
//    NSError *error;
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"favorites.plist"];
//    
//    success = [fileManager fileExistsAtPath:filePath];
//    if (success) return;
//    
//    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"favorites.plist"];
//    success = [fileManager copyItemAtPath:path toPath:filePath error:&error];
//    
//    if (!success) {
//        NSAssert(0, @"Failed to copy plist. Error %@", [error localizedDescription]);
//    }
//}

@end
