//
//  KTADisclaimerViewController.h
//  KidsTripAdvisor
//
//  Created by Maris on 18/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface KTAMapViewController : UIViewController <UITextFieldDelegate, UITextInputDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UITextField *postcodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@property(strong,nonatomic) UITextField *activeTextField;
- (IBAction)clickSearchBtn:(id)sender;

@end
