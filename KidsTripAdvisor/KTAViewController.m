
//
//  KTAViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//
//http://dwilkin.ddns.net/kta
//http://dev1.businessprodemo.com/kta/php

//http://dev1.businessprodemo.com/kta/php

#import "KTAViewController.h"
#import "KTAListViewController.h"
#import "KTARefineSearchViewController.h"
#import "KTAAppDelegate.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ChildsPlayJsonURL [NSURL URLWithString:@"http://dev1.businessprodemo.com/kta/php/downloadJSON.php"]


@interface KTAViewController ()
@end

@implementation KTAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    // set background depending on device screen size
//    UIImage *backgroundImage = [[UIImage alloc] init];
//    if ([UIScreen mainScreen].bounds.size.height > 480.0f) {
//        backgroundImage = [UIImage imageNamed:@"iphone5Background.jpg"];
//    } else {
//        backgroundImage = [UIImage imageNamed:@"iphoneBackground.jpg"];
//    }
    
    // placeholder ad
    self.mainView.backgroundColor = [UIColor whiteColor];
    _iAd.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iAd.png"]];
    
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
    UIImageView *addLabel=[[UIImageView alloc] initWithFrame:CGRectMake(0, 890, [UIScreen mainScreen].bounds.size.width, 70)];
    addLabel.image = [UIImage imageNamed:@"iAd.png"];
    [self.view addSubview:addLabel];
       // UIImageView *imgview=[[UIImageView alloc] initWithFrame:CGRectMake(self.mainView.frame.origin.x, self.mainView.frame.origin.y, self.mainView.frame.size.width, self.mainView.frame.size.height)];
       // imgview.image=backgroundImage;
       // [self.mainView addSubview:imgview];
  
    }
    // When the view loads stop updating users location, SAVES BATTERY
    [locationManager stopUpdatingLocation];
    
   
}
- (void)viewDidLayoutSubviews
{
     [_aScrollvw setContentSize:CGSizeMake(0, 540)];
}
- (IBAction)didTapback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setOpaque:YES];
    
    // Start getting users location
    shouldHideBtn=NO;

    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    indoorContent = [[NSMutableArray alloc] init];
    outdoorContent = [[NSMutableArray alloc] init];
    
    NSString *type;

    for (NSDictionary *dict in _allContent)
    {
        type = [dict objectForKey:@"Type"];

        if ([[type uppercaseString] isEqualToString:@"OUTDOOR"] || [type isEqualToString:@"All"]) [outdoorContent addObject:dict];
        if ([[type uppercaseString] isEqualToString:@"INDOOR"] || [type isEqualToString:@"All"]) [indoorContent addObject:dict];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    KTARefineSearchViewController *vc = [segue destinationViewController];
    
    if ([[segue identifier] isEqualToString:@"outdoorSegue"]) {
        [vc setContent:outdoorContent];
        [vc setTitle:@"Outdoor"];
    }
    
    else if ([[segue identifier] isEqualToString:@"indoorSegue"]) {
        [vc setContent:indoorContent];
        [vc setTitle:@"Indoor"];
    }
    else if ([[segue identifier] isEqualToString:@"allSegue"]) {
        [vc setContent:_allContent];
        [vc setTitle:@"All"];
    }
}

- (IBAction)myCalendarBtn:(id)sender {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you want to leave Kids Trip Advisor?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"calshow://"]];
    }
}

@end
