//
//  KTAAppDelegate.m
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTAAppDelegate.h"

@implementation KTAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    //Customize nav bar
    
    NSLog(@"This is the device type :::::::::::::::%@",[[UIDevice currentDevice] model]);
    
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        // Load IPAD StoryBoard
        UIStoryboard *storyBoard;
        
        storyBoard = [UIStoryboard storyboardWithName:@"Storyboard-iPad" bundle:nil];
        UIViewController *initViewController = [storyBoard instantiateInitialViewController];
        [self.window setRootViewController:initViewController];
        
  
    }
    
//    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
//    {
//        // Load IPAD StoryBoard
//        UIStoryboard *storyBoard;
//        
//        storyBoard = [UIStoryboard storyboardWithName:@"Storyboard-iPad" bundle:nil];
//        UIViewController *initViewController = [storyBoard instantiateInitialViewController];
//        [self.window setRootViewController:initViewController];
//        
//    }

    
    UIImage *navBarImage = [self imageWithColor:[UIColor colorWithRed:194.0/255.0 green:25.0/255.0 blue:49.0/255.0 alpha:1.0] withRect:CGRectMake(0.0, 0.0, 1, 2)];
    [[UINavigationBar appearance] setBackgroundImage:navBarImage forBarMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor,nil]
                                                                                            forState:UIControlStateNormal];
    // image for custom buttons
    UIImage *clearBtnImage = [self imageWithColor:[UIColor clearColor] withRect:CGRectMake(0.0, 0.0, 44, 44)];
    // customize back button
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:clearBtnImage
                                                      forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    // set right nav button
    [[UIBarButtonItem appearance] setBackgroundImage:clearBtnImage forState:UIControlStateNormal
                                          barMetrics:UIBarMetricsDefault];
    // set navigation bar text
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0],
      UITextAttributeTextColor,
      [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0],
      UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
      UITextAttributeTextShadowOffset,
      [UIFont fontWithName:@"Arial-Bold" size:0.0],
      UITextAttributeFont,
      nil]];
    
    /*
     
     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
     UIStoryboard *storyBoard;
     
     CGSize result = [[UIScreen mainScreen] bounds].size;
     CGFloat scale = [UIScreen mainScreen].scale;
     result = CGSizeMake(result.width * scale, result.height * scale);
     
     if(result.height == 1136){
     storyBoard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone_5" bundle:nil];
     UIViewController *initViewController = [storyBoard instantiateInitialViewController];
     [self.window setRootViewController:initViewController];
     }
     }
     */
    
    
    
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (UIImage *)imageWithColor:(UIColor *)color withRect:(CGRect)newRect  {
    CGRect rect = newRect;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
