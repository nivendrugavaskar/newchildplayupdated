//
//  KTADetailViewController.h
//  KidsTripAdvisor
//
//  Created by Maris on 10/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KTARateView.h"

@interface KTADetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RateViewDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIImageView *detailImageView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UITextView *longDescription;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableDictionary *contentDictionary;

@property (weak, nonatomic) IBOutlet KTARateView *rateView;
@property (weak, nonatomic) IBOutlet UILabel *iAd;


@end
