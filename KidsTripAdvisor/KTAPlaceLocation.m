//
//  KTAPlaceLocation.m
//  KidsTripAdvisor
//
//  Created by Beau Young on 16/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTAPlaceLocation.h"

@implementation KTAPlaceLocation

@synthesize coordinate;

- (NSString *)subtitle {
    return nil;
}

- (NSString *)title {
    return nil;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D) c {
    coordinate = c;
    return  self;
}


@end
