//
//  KTAFavoritesViewController.h
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>


@interface KTAFavoritesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *iAd;


@end
