//
//  KTAFavoritesViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTAFavoritesViewController.h"
#import "KTADetailViewController.h"
#import "KTACustomUITableViewCell.h"
#import "KTAAppDelegate.h"
#import "LGViewHUD.h"
#import "AFNetworking.h"
#import "KeychainItemWrapper.h"

#import <SystemConfiguration/SystemConfiguration.h>
#import "HttpAPI.h"
#import "Global.h"
#import "Util.h"
#import "SBJson.h"
#import "SBJsonParser.h"



#import <SDWebImage/UIImageView+WebCache.h>

@interface KTAFavoritesViewController ()
{
    NSMutableArray *favoritesArray;
    CLLocationManager *locationManager;
    NSMutableURLRequest *request;
    NSMutableData *receivedData;
    NSString *DeviceUDID;

}

@end

@implementation KTAFavoritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Delegate set in IB
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    _iAd.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iAd.png"]];
    receivedData = [NSMutableData data];
    
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        UIImageView *addLabel=[[UIImageView alloc] initWithFrame:CGRectMake(0, 890, [UIScreen mainScreen].bounds.size.width, 70)];
        addLabel.image = [UIImage imageNamed:@"iAd.png"];
        [self.view addSubview:addLabel];
    }


    // When view loads, stop updating location. Saves on Battery. Coming back to this view will refresh it.
    [locationManager stopUpdatingLocation];
    
}

- (void)viewWillAppear:(BOOL)animated {
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"favorites.plist"];
//    favoritesArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
//    
//    // update users location.
//    locationManager = [[CLLocationManager alloc] init];
//    locationManager.delegate = self;
//    locationManager.distanceFilter = kCLDistanceFilterNone;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    [locationManager startUpdatingLocation];
//    
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
   self.navigationController.navigationBar.translucent = NO;
//    
   self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self serviceCall:@"" :@""];
   // [self doconnection];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Search and load document
    return [favoritesArray count];
}

- (KTACustomUITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *newContent;
//    if (tableView == self.searchDisplayController.searchResultsTableView) newContent = [favoritesArray objectAtIndex:indexPath.row];
//    else newContent = [favoritesArray objectAtIndex:indexPath.row];
    newContent=[favoritesArray objectAtIndex:indexPath.row];

    NSMutableDictionary *parsedResponse=[[NSMutableDictionary alloc] init];
    parsedResponse=[newContent objectAtIndex:0];
    
    
    
    // This is also set on the cell in the storyboard.
    static NSString *CellIdentifier = @"Cell";
    KTACustomUITableViewCell *cell = (KTACustomUITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[KTACustomUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle  reuseIdentifier:CellIdentifier];
    }
    // Set location name from plist
    cell.titleTextLabel.text = [parsedResponse valueForKey:@"CompanyName"];
    
    // set category label
    cell.categoryTextLabel.text = [NSString stringWithFormat:@"%@", [parsedResponse valueForKey:@"GroupClassification"]];
    
    // set suburb label
    cell.suburbTextLabel.text = [NSString stringWithFormat:@"%@", [parsedResponse valueForKey:@"City"]];
    
    // create string of location in KM (default is Meters)
    cell.distanceTextLabel.text = [parsedResponse valueForKey:@"DistanceForSort"];
    
    // set image
    [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://dev1.businessprodemo.com/kta/php/appimgs/%@/front.png", [parsedResponse valueForKey:@"store_id"]]] placeholderImage:[UIImage imageNamed:@"NoImageIcon"]];
    
    // Extra customizeation
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    // set cell background image
    UIImageView *cellImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellBackgroundImage"]];
    [cell setBackgroundView:cellImage];
    
    // Check if premium
    if ([[parsedResponse valueForKey:@"Premium"] isEqualToString:@"YES"])
    {
        cell.isPremium = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Perform segue to detail view
//    [self performSegueWithIdentifier:@"detailViewSegue" sender:tableView];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    if (editing == YES) {
        NSLog(@"editing");
        
    } else {
        NSLog(@"not editing");
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *contentArr=[[NSArray alloc] init];
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        contentArr=[favoritesArray objectAtIndex:indexPath.row];
        [favoritesArray removeObjectAtIndex:indexPath.row];

        
    }
// Fetch favorites plist with data
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentDirectory stringByAppendingPathComponent:@"favorites.plist"];
//    
//    // Write data to favorites plist
//    [favoritesArray writeToFile:path atomically:YES];
//Pass the name of the method and also see the
    NSMutableDictionary *dictContent=[[NSMutableDictionary alloc]init];
    dictContent=[contentArr objectAtIndex:0];
    NSLog(@"This is the dictioanry containing data to be deleted:::::::::::::%@",dictContent);
    
    [self serviceCall:@"delete" :[NSString stringWithFormat:@"%@",[dictContent valueForKey:@"store_id"]]];
    
    
    [tableView reloadData];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"detailViewSegue"]) {
        NSArray *newContent;
        shouldHideBtn=YES;
        KTADetailViewController *vc = [segue destinationViewController];
        
        if (sender == self.searchDisplayController.searchResultsTableView) {
            NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            newContent=[favoritesArray objectAtIndex:indexPath.row];
            
            NSMutableDictionary *parsedResponse=[[NSMutableDictionary alloc] init];
            parsedResponse=[newContent objectAtIndex:0];

            [vc setContentDictionary:parsedResponse];
            
        } else {
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            newContent=[favoritesArray objectAtIndex:indexPath.row];
            
            NSMutableDictionary *parsedResponse=[[NSMutableDictionary alloc] init];
            parsedResponse=[newContent objectAtIndex:0];

            [vc setContentDictionary:parsedResponse];
            
        }
    }
}

#pragma mark-Service Call Method
-(void)serviceCall:(NSString *)serviceName :(NSString *)storeId
{
    
    {
        KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"Cruzze" accessGroup:nil];
        NSLog(@"TOKEN:%@",[keychain objectForKey:(__bridge id)(kSecValueData)]);
        NSLog(@"USER NAME:%@",[keychain objectForKey:(__bridge id)(kSecAttrAccount)]);
        DeviceUDID=[keychain objectForKey:(__bridge id)(kSecValueData)];
        
        
        if (([DeviceUDID length]<1)||([DeviceUDID isEqualToString:@""]))
        {
            DeviceUDID = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].identifierForVendor.UUIDString];
            KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"Cruzze" accessGroup:nil];
            [keychain setObject:DeviceUDID forKey:(__bridge id)(kSecValueData)];
//            [keychain setObject:txtbxUserName.text forKey:(__bridge id)(kSecAttrAccount)];

            
        }
        
    }
    
    
    
    NSMutableDictionary *parameters;
   NSString *url = [NSString stringWithFormat:@"%@",@"http://dev1.businessprodemo.com/kta/php/favorite.php"];
    if ([serviceName isEqualToString:@"delete"])
    {
        parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:storeId,@"store_id",
                                           DeviceUDID,@"device_id",serviceName,@"type",
                                           nil];
        NSLog(@"This is the service parameters:::::%@",parameters);
        //
        LGViewHUD* hud = [LGViewHUD defaultHUD];
        hud.activityIndicatorOn=YES;
        hud.topText=@"";
        hud.bottomText=@"";
        [hud showInView:self.view];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"successful %@", responseObject);
            
            NSMutableArray *responseArr=[[NSMutableArray alloc] init];
            responseArr=[responseObject objectForKey:@"details"];
            NSLog(@"Thisis the count of fav array::::: %d",[responseArr count]);
            
            //        favoritesArray =[[NSMutableArray alloc] initWithObjects:[responseArr objectAtIndex:0], nil];
//            favoritesArray=[responseArr mutableCopy];
            
            NSLog(@"Thisis the fvourites array...... %@",favoritesArray);
            NSLog(@"Thisis the count of fav array::::::::: %d",[favoritesArray count]);
            
            
            [self.tableView reloadData];
            
            [[LGViewHUD defaultHUD] hideWithAnimation:HUDAnimationHideFadeOut];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"failed");
            
            [[LGViewHUD defaultHUD] hideWithAnimation:HUDAnimationHideFadeOut];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Network Error!" message:@"Please Check Network Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            
            
        }];

        
        
    }
    else
    {
        parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"",@"store_id",
                      DeviceUDID,@"device_id",@"get",@"type",
                      nil];
        NSLog(@"This is the service parameters:::::%@",parameters);
        LGViewHUD* hud = [LGViewHUD defaultHUD];
        hud.activityIndicatorOn=YES;
        hud.topText=@"";
        hud.bottomText=@"";
        [hud showInView:self.view];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
            
            NSLog(@"successful %@", responseObject);
            
            NSMutableArray *responseArr=[[NSMutableArray alloc] init];
            responseArr=[responseObject objectForKey:@"details"];
             if (![responseArr isKindOfClass:[NSNull class]])
             {
                 NSLog(@"Thisis the count of fav array::::: %d",[responseArr count]);
                 
                 //        favoritesArray =[[NSMutableArray alloc] initWithObjects:[responseArr objectAtIndex:0], nil];
                 favoritesArray=[responseArr mutableCopy];
                 
                 NSLog(@"Thisis the fvourites array...... %@",favoritesArray);
                 NSLog(@"Thisis the count of fav array::::::::: %d",[favoritesArray count]);
                 
                 
                 [self.tableView reloadData];
             }
             
           
            
            [[LGViewHUD defaultHUD] hideWithAnimation:HUDAnimationHideFadeOut];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"failed");
            
            [[LGViewHUD defaultHUD] hideWithAnimation:HUDAnimationHideFadeOut];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Network Error!" message:@"Please Check Network Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            
            
        }];

        
    }
    
//

    
        
        
}

#pragma mark-Delegate method forservice call

#pragma mark-methods for calling the service and storing the values of journey in service and local database

-(void)doconnection
{
    //   alert=[[UIAlertView alloc]initWithTitle:@"Debug" message:@" Service Being Called ..Line 1666" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //   [alert show];
   
    NSURL *url;
    NSString *dataString;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"",@"store_id",
                                       @"112454sdafasf",@"device_id",@"get",@"type",
                                       nil];
    
    //
    NSLog(@"This is the service parameters:::::%@",parameters);
    url = [NSURL URLWithString: @"http://dev1.businessprodemo.com/kta/php/favorite.php"];
    request = [NSMutableURLRequest requestWithURL:url];
    
    dataString=[parameters JSONRepresentation];
    NSLog(@"my data string %@",dataString);
   // NSData *data=[EDZ gzipDeflate:[dataString dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *data=[[NSData alloc] initWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"request body %@",data);
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d", [data length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: data];
    request.timeoutInterval=60;
    //  [request retain];
//    NSString *cmpressedDataStr =[data base64EncodingWithLineLength:[data length]];
//    NSLog(@"%@", cmpressedDataStr);
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    if(connection)
    {
        NSLog(@"http://dev1.businessprodemo.com/kta/php/favorite.php");
    }
    
  }
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
    //NSString *responseStr = [response JSONRepresentation];
    httpResponse = (NSHTTPURLResponse *) response;
    //    [httpResponse retain];
    NSLog(@"%d", httpResponse.statusCode);
    NSLog(@"%@",[httpResponse description]);
    //      alert=[[UIAlertView alloc]initWithTitle:@"Debug" message:@" Service recieved response Called ..Line 1684" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    //  [alert show];
    
    
    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    //[MBProgressHUD hideHUDForView:self.view animated:YES];
//    receivedData=[[NSMutableData alloc] init];
    [receivedData appendData:data];
    //   [receivedData retain];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error.description);
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSLog(@"Response string %@",responseString);

}




@end
