//
//  KTAPlaceLocation.h
//  KidsTripAdvisor
//
//  Created by Beau Young on 16/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface KTAPlaceLocation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D) c;

@end
