//
//  KTARefineSearchViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 12/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTARefineSearchViewController.h"

@interface KTARefineSearchViewController ()

@end

@implementation KTARefineSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
