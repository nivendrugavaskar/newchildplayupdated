//
//  KTARateView.h
//  KidsTripAdvisor
//
//  Created by Beau Young on 18/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KTARateView;

@protocol RateViewDelegate
- (void)rateView:(KTARateView *)rateView ratingDidChange:(float)rating;

@end

@interface KTARateView : UIView

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;

@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (strong) NSMutableArray *imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <RateViewDelegate> delegate;

@end
