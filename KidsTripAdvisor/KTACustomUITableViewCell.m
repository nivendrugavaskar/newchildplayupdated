//
//  KTACustomUITableViewCell.m
//  KidsTripAdvisor
//
//  Created by Maris on 10/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTACustomUITableViewCell.h"

@implementation KTACustomUITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    float desiredWidth = 80;
//    float w = self.imageView.frame.size.width;
//    if (w > desiredWidth) {
//        float widthSub = w - desiredWidth;
//        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y, desiredWidth, self.imageView.frame.size.height);
//        self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x-widthSub, self.textLabel.frame.origin.y, self.textLabel.frame.size.width + widthSub, self.textLabel.frame.size.height);
//        self.detailTextLabel.frame = CGRectMake(self.detailTextLabel.frame.origin.x-widthSub, self.detailTextLabel.frame.origin.y, self.detailTextLabel.frame.size.width + widthSub, self.detailTextLabel.frame.size.height);
//        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    }


    // Cell customization
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    self.imageView.frame = CGRectMake(5, 15, 80, 50);
    [self.imageView.layer setCornerRadius:10.0f];
    [self.imageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.imageView.layer setShadowOpacity:0.5];
    [self.imageView.layer setShadowRadius:1.0];
    [self.imageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        
        self.imageView.frame = CGRectMake(5, 15, 125, 70);
        [self.imageView.layer setCornerRadius:10.0f];
        [self.imageView.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.imageView.layer setShadowOpacity:0.5];
        [self.imageView.layer setShadowRadius:1.0];
        [self.imageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
        
        
        
    }

    // Headline Customization
    self.titleTextLabel.adjustsFontSizeToFitWidth = YES;
    self.titleTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];

    // Suburb Customization
    self.suburbTextLabel.adjustsFontSizeToFitWidth = YES;
    self.suburbTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    // Category Customization
    self.categoryTextLabel.adjustsFontSizeToFitWidth = YES;
    self.categoryTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    //Code to check for ipad.../////
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        
        self.titleTextLabel.adjustsFontSizeToFitWidth = YES;
        self.titleTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
        
        // Suburb Customization
        self.suburbTextLabel.adjustsFontSizeToFitWidth = NO;
        self.suburbTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:19];
        
        // Category Customization
        self.categoryTextLabel.adjustsFontSizeToFitWidth = NO;
        self.categoryTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:19];

        
        
    }
    
    
    
    if (_isPremium) {
        [self makePremium];
    }
}


- (void)makePremium {
    NSLog(@"this is premium");
    // apply premium customizations here
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
