//
//  KTADetailViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 10/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTADetailViewController.h"
#import "KTACustomUITableViewCell.h"
#import "KTACalendarViewController.h"
#import "KTAAppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AFNetworking.h"
#import "LGViewHUD.h"
#import "KeychainItemWrapper.h"



#define ChildsPlayJsonURL [NSURL URLWithString:@"http://dev1.businessprodemo.com/kta/php/downloadJSON.php"]

#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>

@interface KTADetailViewController () <MFMailComposeViewControllerDelegate, UIAlertViewDelegate> {
    NSString *phoneNumber;
    NSString *emailAddress;
    NSString *DeviceUDID;
}

@end

@implementation KTADetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.rateView.notSelectedImage = [UIImage imageNamed:@"noStarIcon"];
    self.rateView.fullSelectedImage = [UIImage imageNamed:@"fullStarIcon"];
    self.rateView.rating = 0;
    self.rateView.editable = YES;
    self.rateView.maxRating = 5;
    self.rateView.delegate = self;
    self.rateView.leftMargin = 30;
    self.rateView.midMargin = 10;
    
    _iAd.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iAd.png"]];
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        UIImageView *addLabel=[[UIImageView alloc] initWithFrame:CGRectMake(0, 890, [UIScreen mainScreen].bounds.size.width, 70)];
        addLabel.image = [UIImage imageNamed:@"iAd.png"];
        [self.view addSubview:addLabel];
    }

    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    // Set image
    [self.detailImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://dev1.businessprodemo.com/kta/php/appimgs/%@/back.jpg", [_contentDictionary valueForKey:@"store_id"]]] placeholderImage:[UIImage imageNamed:@"NoImageBanner.png"]];
    
    // set location label
    self.locationLabel.text = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"CompanyName"]];
    
    // set description label
    self.longDescription.text = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"LongDescription"]];
    
    [self.scrollView setContentSize:self.contentView.frame.size];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Some of this is also set on the cell in the storyboard.
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        if (indexPath.row == 0) {   ///   MAP CELL
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mapCell"];
            [cell.textLabel setText:nil];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"Address"]];
            cell.detailTextLabel.font=[UIFont systemFontOfSize:19];
            return cell;
            
        } else if (indexPath.row == 1){   ///// WEB CELL
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"webCell"];
            [cell.textLabel setText:nil];
            
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"WebSite"]];
            cell.detailTextLabel.font=[UIFont systemFontOfSize:19];

            return cell;
            
        } else if (indexPath.row == 2) {     //// PHONE CELL//////////
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"phoneCell"];
            [cell.textLabel setText:nil];
            
            phoneNumber = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"Telephone1"]];
            
            cell.detailTextLabel.text = phoneNumber;
            cell.detailTextLabel.font=[UIFont systemFontOfSize:19];

            return cell;
            
        } else if (indexPath.row == 3) {   //// EMAIL CELL
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"emailCell"];
            [cell.textLabel setText:nil];
            
            emailAddress = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"EmailAddress"]];
            cell.detailTextLabel.text = emailAddress;
            cell.detailTextLabel.font=[UIFont systemFontOfSize:19];

            return cell;
            
        } else if (indexPath.row == 4) {  // Add to favorites
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"favoritesCell"];
            [cell.textLabel setText:nil];
            
            [cell.detailTextLabel setText:@"Add to Favorites"];
            cell.detailTextLabel.font=[UIFont systemFontOfSize:19];
            if (shouldHideBtn)
            {
                cell.detailTextLabel.textColor=[UIColor grayColor];
            }

            return cell;
            
        } else if (indexPath.row == 5) {  // Add to calendar
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"calendarCell"];
            [cell.textLabel setText:nil];
            
            [cell.detailTextLabel setText:@"Add to Calendar"];
            cell.detailTextLabel.font=[UIFont systemFontOfSize:19];

            return cell;
        }
    }
    
    if (indexPath.row == 0) {   ///   MAP CELL
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mapCell"];
        [cell.textLabel setText:nil];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"Address"]];
        
        return cell;
        
    } else if (indexPath.row == 1){   ///// WEB CELL
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"webCell"];
        [cell.textLabel setText:nil];

        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"WebSite"]];
        return cell;

    } else if (indexPath.row == 2) {     //// PHONE CELL
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"phoneCell"];
        [cell.textLabel setText:nil];
        
        phoneNumber = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"Telephone1"]];
        
        cell.detailTextLabel.text = phoneNumber;
        return cell;
        
    } else if (indexPath.row == 3) {   //// EMAIL CELL
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"emailCell"];
        [cell.textLabel setText:nil];
        
        emailAddress = [NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"EmailAddress"]];
        cell.detailTextLabel.text = emailAddress;
        return cell;
        
    } else if (indexPath.row == 4) {  // Add to favorites
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"favoritesCell"];
        [cell.textLabel setText:nil];
        
        [cell.detailTextLabel setText:@"Add to Favorites"];
        return cell;
        
    } else if (indexPath.row == 5) {  // Add to calendar
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"calendarCell"];
        [cell.textLabel setText:nil];
        
        [cell.detailTextLabel setText:@"Add to Calendar"];
        return cell;
    }
    return nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([[segue identifier] isEqualToString:@"calendarView"]) {
        KTACalendarViewController *vc = [segue destinationViewController];
        
        // set contents array on calendar page
        [vc setSingleDetails:_contentDictionary];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Open in maps app of choice
    if (indexPath.row == 0) {                           ///// MAPS
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"cancel" destructiveButtonTitle:nil otherButtonTitles:@"Apple Maps", @"Google Maps", nil];
        [actionSheet showInView:self.view];
    }
    
    if (indexPath.row == 1) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [_contentDictionary valueForKey:@"WebSite"]]];
        
        [[UIApplication sharedApplication] openURL:url];
    }
    
    // Call number when user taps on phone cell
    if (indexPath.row == 2) {                           ///// PHONE
        // Characters to be removed from displayed phone number
        NSCharacterSet *unwantedCharacters = [NSCharacterSet characterSetWithCharactersInString:@" ()-+"];
        // Create new string with characters removed, ready to be used in the calling app
        NSString *newString = [[phoneNumber componentsSeparatedByCharactersInSet:unwantedCharacters] componentsJoinedByString:@""];
        NSURL *pn = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", newString]];
        // open link in calling app
        [[UIApplication sharedApplication] openURL:pn];
        
    }
    // open mail app when user taps on email cell
    if (indexPath.row == 3) {                           ///// EMAIL
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
            [composeViewController setMailComposeDelegate:self];

            // set recipients as email address inside tapped cell
            [composeViewController setToRecipients:@[emailAddress]];
            [composeViewController setSubject:nil];
            // Show mail view controller for composing
            [self presentViewController:composeViewController animated:YES completion:nil];
        }
    }
    if (indexPath.row == 4) {
        ///// FAVORITES
        if (shouldHideBtn==YES)
        {
            NSLog(@"Item already added......");
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"The store is already added in your favourite list. " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [tableView reloadData];
        }
        else
        {
        [self addToFavorites];
       
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // remove mail view controller after dismissing or finishing composition
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)rateView:(KTARateView *)rateView ratingDidChange:(float)rating {
    // send rating to server for the location.
    NSLog(@"rating: %f", rating);
}

#pragma mark - Action sheet for map selection
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Coordinates for the location
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(
        [[_contentDictionary valueForKey:@"Latitude"] doubleValue],
        [[_contentDictionary valueForKey:@"Longitude"] doubleValue]);

    if (buttonIndex == 0) {
        // Apple Maps using the MapItem class
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
        
        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
        item.name = [_contentDictionary valueForKey:@"CompanyName"];
        [item openInMapsWithLaunchOptions:nil];
        
    } else if (buttonIndex == 1) {
        // Google Maps
        // Construct a URL using the comgooglemaps schema
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=%f,%f", location.latitude, location.longitude]];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
            NSLog(@"Google Maps can open");
            [[UIApplication sharedApplication] openURL:url];

        } else {  // If Google maps is not installed, open in web browser instead.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Google Maps is not installed on this device. Would you like to open the location in Safari?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes Please", nil];
            [alert show];
        }
    }
}
#pragma mark - Open in google maps safari
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(
        [[_contentDictionary valueForKey:@"Latitude"] doubleValue],
        [[_contentDictionary valueForKey:@"Longitude"] doubleValue]);
    
    if (buttonIndex == 1) {
        NSLog(@"user pressed YES");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.google.com/maps?q=%f,%f", location.latitude, location.longitude]]];
    }
}
// Add location to favorites
- (void)addToFavorites {
    // Fetch plist with data
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentDirectory stringByAppendingPathComponent:@"favorites.plist"];
//    NSMutableArray *mutableArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
//   // NSMutableArray *mutableArray=[[NSMutableArray alloc] init];
//    [mutableArray addObject:_contentDictionary];
//    
//    if (mutableArray) {
//        // Write data to plist
//        [mutableArray writeToFile:path atomically:YES];
//        NSLog(@"New plist content:%@", mutableArray);
//        
//        
//    }else
//    {
//    NSMutableArray *mutableArray=[[NSMutableArray alloc] init];
//        [mutableArray writeToFile:path atomically:YES];
//        NSLog(@"New plist content:%@", mutableArray);
//
   
//    }
    
    
    NSLog(@"This is the dictionary of the restaurant:::::::::::%@",_contentDictionary);
    [self serviceCall];
    
}

-(void)serviceCall
{
    
    KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"Cruzze" accessGroup:nil];
    NSLog(@"TOKEN:%@",[keychain objectForKey:(__bridge id)(kSecValueData)]);
    NSLog(@"USER NAME:%@",[keychain objectForKey:(__bridge id)(kSecAttrAccount)]);
    
    
    DeviceUDID =[keychain objectForKey:(__bridge id)(kSecValueData)];
    
    
    if (([DeviceUDID length]<1) ||([DeviceUDID isEqualToString:@""]))
    {
        DeviceUDID = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].identifierForVendor.UUIDString];
        
        KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"Cruzze" accessGroup:nil];
        [keychain setObject:DeviceUDID forKey:(__bridge id)(kSecValueData)];
    }

    
    
    
    NSString *url = [NSString stringWithFormat:@"%@",@"http://dev1.businessprodemo.com/kta/php/favorite.php"];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[_contentDictionary valueForKey:@"store_id"],@"store_id",
                                       DeviceUDID,@"device_id",@"add",@"type",
                                       nil];
    //2815,[_contentDictionary valueForKey:@"store_id"]
    NSLog(@"This is the service parameters:::::%@",parameters);
    LGViewHUD* hud = [LGViewHUD defaultHUD];
    hud.activityIndicatorOn=YES;
    hud.topText=@"";
    hud.bottomText=@"";
    [hud showInView:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"successful %@", responseObject);
        
        NSMutableArray *responseArr=[[NSMutableArray alloc] init];
        responseArr=[responseObject objectForKey:@"details"];
        NSLog(@"Thisis the count of fav array::::: %d",[responseArr count]);
        
        //        favoritesArray =[[NSMutableArray alloc] initWithObjects:[responseArr objectAtIndex:0], nil];
//        favoritesArray=[responseArr mutableCopy];
        
//        NSLog(@"Thisis the fvourites array...... %@",favoritesArray);
//        NSLog(@"Thisis the count of fav array::::::::: %d",[favoritesArray count]);
        UIAlertView *alert;
        if ([[[responseObject valueForKey:@"status"] stringValue] isEqualToString:@"1"])
        {
        
//            alert=[[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            alert = [[UIAlertView alloc] initWithTitle:nil message:@"Added to Favorites" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
        else{
            alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"The store is already added." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
        
       [self.tableView reloadData];
        
        [[LGViewHUD defaultHUD] hideWithAnimation:HUDAnimationHideFadeOut];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"failed");
        
        [[LGViewHUD defaultHUD] hideWithAnimation:HUDAnimationHideFadeOut];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Network Error!" message:@"Please Check Network Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
        
    }];
    
    
    
    
}



@end
