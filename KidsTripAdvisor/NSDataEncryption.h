//
//  NSDataEncryption.h
//  EncryptPassword
//
//  Created by rubela shome on 04/05/13.
//  Copyright (c) 2013 rubela shome. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)

- (NSData *)AES128EncryptWithKey:(NSString *)key;
- (NSData *)AES128DecryptWithKey:(NSString *)key;

@end
