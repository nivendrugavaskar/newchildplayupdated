//
//  KTADisclaimerViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 18/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTAMapViewController.h"
#import "KTAViewController.h"
#import "KTAAppDelegate.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ChildsPlayJsonURL [NSURL URLWithString:@"http://dev1.businessprodemo.com/kta/php/downloadJSON.php?state=QLD"]
#define ChildPlayJsonURLstring @"http://dev1.businessprodemo.com/kta/php/downloadJSON.php"


@interface KTAMapViewController () {
    NSMutableArray *content;
    NSString *stateSelected;
    NSString *postcodeSelected;
    float lat;
    float lon;
    int locationCounter;
    CGPoint pointtrack,pointtrackcontentoffset;

}

@end

@implementation KTAMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#pragma set fame to manage animation
    pointtrackcontentoffset=self.view.frame.origin;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.postcodeTextField.delegate = self;
    
	// Do any additional setup after loading the view.
    // set background depending on device screen size
    UIImage *backgroundImage = [[UIImage alloc] init];
    if ([UIScreen mainScreen].bounds.size.height > 480.0f) backgroundImage = [UIImage imageNamed:@"iphone5Background.jpg"];
    else backgroundImage = [UIImage imageNamed:@"iphoneBackground.jpg"];
    
    if ([[[UIDevice currentDevice] model] containsString:@"iPad"])
    {
        backgroundImage=[UIImage imageNamed:@"bg_iPad.png"];
//        self.mainView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
//        UIImageView *imgview=[[UIImageView alloc] initWithFrame:CGRectMake(self.mainView.frame.origin.x, self.mainView.frame.origin.y, self.mainView.frame.size.width, self.mainView.frame.size.height)];
//        imgview.image=backgroundImage;
//        [self.mainView addSubview:imgview];
//    UIImageView *imgview= [[UIImageView alloc] initWithFrame:CGRectMake(self.mainView.frame.origin.x, self.mainView.frame.origin.y, self.mainView.frame.size.width, self.mainView.frame.size.height)];
//        imgview.image=backgroundImage;
//        
//        self.view=imgview;
    }
    
    // locationManager update as location
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
//    CLLocation *location = [locationManager location];
    // Configure the new event with information from the location

    
//    lon = location.coordinate.longitude;
//    lat = location.coordinate.latitude;
//    NSLog(@"This is the latitude and longitude:::::::%f %f",lat,lon);
//    
    //new code for location checking////
    [self deviceLocation];
    
    NSLog(@"this is the os version %0.04f",[[[UIDevice currentDevice] systemVersion] floatValue]);
    
    
    if ([self compareVersionString:[UIDevice currentDevice].systemVersion
                 withVersionString:@"8.0"] == NSOrderedAscending  ||[[[UIDevice currentDevice] systemVersion] floatValue]==8.0 |[[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
    {
        [self updateLocation];
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardhide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardhide:(NSNotification*)notification
{
    [self dismissKeyboard];
}
-(void)dismissKeyboard
{
    //[self.view endEditing:YES];
    [self.activeTextField resignFirstResponder];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
}
#pragma mark-SYSTEM OS VERSION COMPARISION METHOD


- (NSComparisonResult)compareVersionString:(NSString *)versionString1
                         withVersionString:(NSString *)versionString2
{
    NSArray *components1 = [versionString1 componentsSeparatedByString:@"."];
    NSArray *components2 = [versionString2 componentsSeparatedByString:@"."];
    NSUInteger components1Count = [components1 count];
    NSUInteger components2Count = [components2 count];
    NSUInteger partCount = MAX(components1Count, components2Count);
    
    for (NSInteger part = 0; part < partCount; ++part)
    {
        if (part >= components1Count)
        {
            return NSOrderedAscending;
        }
        
        if (part >= components2Count)
        {
            return NSOrderedDescending;
        }
        
        NSString *part1String = components1[part];
        NSString *part2String = components2[part];
        NSInteger part1 = [part1String integerValue];
        NSInteger part2 = [part2String integerValue];
        
        if (part1 > part2)
        {
            return NSOrderedDescending;
        }
        if (part1 < part2)
        {
            return NSOrderedAscending;
        }
    }
    return NSOrderedSame;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self dismissKeyboard];
    KTAViewController *vc=[segue destinationViewController];
    
    if ([[segue identifier] isEqualToString:@"QLDSegue"]) {
        
        vc.isState = YES;
        [self createContentForState:@"?state=QLD"];
        [vc setAllContent:content];
        [vc setState:@"QLD"];
        [vc setTitle:@"QLD"];
    }
    else if ([[segue identifier] isEqualToString:@"VICSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=VIC"];
        [vc setAllContent:content];
        [vc setState:@"VIC"];
        [vc setTitle:@"VIC"];
    }
    else if ([[segue identifier] isEqualToString:@"TASSegue"]) {
        vc.isState = YES;
        
        [self createContentForState:@"?state=TAS"];
        [vc setAllContent:content];
        [vc setState:@"TAS"];
        [vc setTitle:@"TAS"];
    }
    else if ([[segue identifier] isEqualToString:@"NTSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=NT"];
        [vc setAllContent:content];
        [vc setState:@"NT"];
        [vc setTitle:@"NT"];
    }
    else if ([[segue identifier] isEqualToString:@"ACTSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=ACT"];
        [vc setAllContent:content];
        [vc setState:@"ACT"];
        [vc setTitle:@"ACT"];
    }
    else if ([[segue identifier] isEqualToString:@"SASegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=SA"];
        [vc setAllContent:content];
        [vc setState:@"SA"];
        [vc setTitle:@"SA"];
    }
    else if ([[segue identifier] isEqualToString:@"WASegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=WA"];
        [vc setAllContent:content];
        [vc setState:@"WA"];
        [vc setTitle:@"WA"];
    }
    else if ([[segue identifier] isEqualToString:@"NSWSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=NSW"];
        [vc setAllContent:content];
        [vc setState:@"NSW"];
        [vc setTitle:@"NSW"];
    }
    else if ([[segue identifier] isEqualToString:@"nearmeSegue"]) {
        vc.isLocation = YES;
        [self createContentWithLatitiude:lat longitude:lon];
        [vc setAllContent:content];
        [vc setTitle:@"Near me"];
    }
    else if ([[segue identifier] isEqualToString:@"postcodeSegue"]) {
        vc.isPostcode = YES;
        self.postcodeTextField.text = nil;
       // self.searchBtn.hidden = YES;
        [vc setAllContent:content];
        [vc setTitle:@"Postcode"];
    }
  
   

}
/*
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
   

    if ([identifier isEqualToString:@"postcodeSegue"]) {
        vc.isPostcode = YES;
        [self createContentWithPostcode:self.postcodeTextField.text];
        [self.postcodeTextField resignFirstResponder];
        if ([[[content valueForKey:@"status"] stringValue] isEqualToString:@"0"])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:[NSString stringWithFormat:@"%@",[content valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        self.postcodeTextField.text = nil;
        self.searchBtn.hidden = YES;
        [vc setAllContent:content];
        [vc setTitle:@"Postcode"];
    }
    
    return YES;
}
 */

// Takes a state and adds it onto the pre defined URL, then calls the "fetchedData" method
- (void)createContentForState:(NSString *)state {
    
    NSURL *newURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ChildPlayJsonURLstring, state]];
    NSLog(@"This is the url of service::::::%@",newURL);
    NSData *data = [NSData dataWithContentsOfURL:newURL];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data
                        waitUntilDone:YES];
}


- (void)createContentWithPostcode:(NSString *)postcode {
    NSURL *newURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?postcode=%@", ChildPlayJsonURLstring, postcode]];
    NSLog(@"This is the url of service::::::%@",newURL);

    NSData *data = [NSData dataWithContentsOfURL:newURL];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data
                        waitUntilDone:YES];
}


- (void)createContentWithLatitiude:(double)latitude longitude:(double)longitude {
    
    NSURL *newURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?loc=%f,%f", ChildPlayJsonURLstring, latitude, longitude]];
    NSLog(@"This is the url of service::::::%@",newURL);

    NSData *data = [NSData dataWithContentsOfURL:newURL];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data
                        waitUntilDone:YES];
}

// Takes in postcode of users location
#pragma mark - JSON Parsing method
- (void)fetchedData:(NSData *)responseData {
    // parse out the json data
    NSError *error;
    
    content = [NSJSONSerialization JSONObjectWithData:responseData
                                              options:kNilOptions
                                                error:&error];
    
}

#pragma mark - TextField Delegate / Protocl Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.activeTextField=textField;
    //self.searchBtn.hidden = NO;
    pointtrack = textField.superview.frame.origin;
    
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(SCREEN_HEIGHT==480)
    {
     rect.origin.y-=pointtrack.y-160;
    }
    else if (SCREEN_HEIGHT==568)
    {
      rect.origin.y-=pointtrack.y-260;
    }
    else if (SCREEN_HEIGHT==1024)
    {
       rect.origin.y-=pointtrack.y-600;
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self dismissKeyboard];
    return YES;
}

- (void)textDidChange:(id<UITextInput>)textInput {
    
}

- (void)selectionDidChange:(id<UITextInput>)textInput {
    
}

- (void)selectionWillChange:(id<UITextInput>)textInput {
    
}

- (void)textWillChange:(id<UITextInput>)textInput{
    
}


- (IBAction)clickSearchBtn:(id)sender
{
    [self dismissKeyboard];
    if ([self.postcodeTextField.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter some zip code." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        //[self.postcodeTextField resignFirstResponder];
       // return;

    }
    else
    {
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([self.postcodeTextField.text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        [self createContentWithPostcode:self.postcodeTextField.text];
        [self.postcodeTextField resignFirstResponder];
        if ([content isKindOfClass:[NSArray class]])
        {
            NSLog(@"hello ihjiohioiohji");
            [self performSegueWithIdentifier: @"postcodeSegue" sender: self];
            
        }
        else if ([[[content valueForKey:@"status"] stringValue] isEqualToString:@"0"])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:[NSString stringWithFormat:@"%@",[content valueForKey:@"message"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        else
        {
            [self performSegueWithIdentifier: @"postcodeSegue" sender: self];
        }

        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter some valid zip code." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       // [self.postcodeTextField resignFirstResponder];
        return;

    }
}

    
//    self.postcodeTextField.text = nil;
//    self.searchBtn.hidden = YES;
//    [vc setAllContent:content];
//    [vc setTitle:@"Postcode"];

    
}


#pragma mark -method for location manager

-(void)updateLocation
{
    NSLog(@"Location mangaer method entered::::::");
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
        
    }
    
}
- (void)deviceLocation
{
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    NSLog(@"%@",[error domain]);
    if ([[error domain] isEqualToString: kCLErrorDomain] && (([error code] == kCLErrorLocationUnknown)||([error code] == kCLErrorDenied))) {
        // The user denied your app access to location information.
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        UIAlertView *errorAlert = [[UIAlertView alloc]
                                   initWithTitle:@"Location Error" message:@"To Enable the Location manager follow the steps: Settings->Privacy->Location-> Enable the app" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
        
    }
    
    NSLog(@"didFailWithError: %@", error);
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
   
    NSLog(@"Getting Location");
    NSLog(@"the loctions being recieved are::::::%@",locations);
    CLLocation *currentLocation = [locations lastObject];
    lat=currentLocation.coordinate.latitude;
    lon=currentLocation.coordinate.longitude;
    NSLog(@"Thisis the lat longitiude of the user::::::::::::%f %f",lat,lon);
    //CLLocation *newLocation=;
    //NSString *lat = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.latitude];
    //NSString *lon = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.longitude];
    if (locationCounter>2)
    {
        [locationManager stopUpdatingLocation];

    }
    locationCounter++;
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"");
    NSLog(@"");
    NSLog(@"");
    lat=newLocation.coordinate.latitude;
    lon=newLocation.coordinate.longitude;
    
    NSLog(@"Thisis the lat longitiude of the user::::::::::::%f %f",lat,lon);
    //CLLocation *newLocation=;
    //NSString *lat = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.latitude];
    //NSString *lon = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.longitude];
    if (locationCounter>2)
    {
        [locationManager stopUpdatingLocation];
        
    }
    locationCounter++;
    
 
}


@end
