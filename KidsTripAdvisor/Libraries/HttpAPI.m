//
//  HttpAPI.m
//  BarcodeApp
//
//  Created by iOSDev on 3/4/14.
//  Copyright (c) 2014 iOSDev. All rights reserved.
//

#import "HttpAPI.h"
#import "JSON.h"
#import "Global.h"
#import "Util.h"


@interface HttpAPI()

@end

@implementation HttpAPI

+ (void) sendRequest:(BOOL) isPost paramDic:(NSMutableDictionary*)paramDic url:(NSString *)url  completionBlock: (void (^)(BOOL, NSDictionary *, NSError *))completionBlock {
    
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    
    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
   NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
  //  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl];
    
     NSLog(@"------  URl: %@", aUrl);
    
    if (isPost) {
        [request setHTTPMethod:@"POST"];
    } else {
        [request setHTTPMethod:@"GET"];
    }
    
    NSString *postString = @"";
    
    postString = [paramDic JSONRepresentation];

    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
   // [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postString dataUsingEncoding:NSUTF8StringEncoding]] forHTTPHeaderField:@"Content-Length"];
    
    
    [Util showIndicator];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               NSString *json_string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                               NSDictionary *resultDic = [parser objectWithString:json_string error:nil];
                               
                           //    [SuperViewController setResponseResult:resultDic];
                               
                             //  NSLog(@"------ API Key: %@", [resultDic objectForKey:@"api_key"]);
                                NSLog(@"------ Result: %@", resultDic);
                               
                               completionBlock(YES, resultDic, error);
                               
                               [Util hideIndicator];
                           }];
}

+ (void) sendPUTRequest:(NSString *) isPost paramDic:(NSMutableDictionary*)paramDic  url:(NSString *)url isFirst:(BOOL)first isLast:(BOOL)last  completionBlock: (void (^)(BOOL, NSDictionary *, NSError *))completionBlock ;
{
    
    
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    
    //    NSURL *aUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",HTTP_BASIC_PARAM, url]];
    NSURL *aUrl = [NSURL URLWithString:url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    //  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl];
    
    NSLog(@"------  URl: %@", aUrl);
    
    //    if (isPost) {
    //        [request setHTTPMethod:@"POST"];
    //    } else {
    //        [request setHTTPMethod:@"GET"];
    //    }
    [request setHTTPMethod:@"PUT"];
    
    NSString *postString = @"";
    
    postString = [paramDic JSONRepresentation];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    //   [request setValue:@"no-cache" forHTTPHeaderField:@"Cache-Control"];
    //  [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postString dataUsingEncoding:NSUTF8StringEncoding]] forHTTPHeaderField:@"Content-Length"];
    
    if (first) {
        [Util showIndicator];
    }
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               NSString *json_string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                               NSDictionary *resultDic = [parser objectWithString:json_string error:nil];
                               
                               //    [SuperViewController setResponseResult:resultDic];
                               
                               //  NSLog(@"------ API Key: %@", [resultDic objectForKey:@"api_key"]);
                               NSLog(@"------ Result: %@", resultDic);
                               
                               completionBlock(YES, resultDic, error);
                               
                               if (last) {
                                   
                                   [Util hideIndicator];
                               }
                               
                           }];
    
}





@end
