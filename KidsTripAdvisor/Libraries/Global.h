//
//  Global.h
//  Baggid
//
//  Created by Ajeet Kumar on 29/04/15.
//  Copyright (c) 2015 Rubela Shome. All rights reserved.
//

#ifndef Baggid_Global_h
#define Baggid_Global_h


#define     HTTP_BASIC_PARAM                 @"http://api.caricode.com/users"
//6b52116f106240eeae56b19149fe35d0/profiles/1/categories/"
#define PROFILEID   @"profileID"

#define NICKNAME    @"nickName"
#define NAME        @"name"
#define EMAIL       @"email"
#define ADDRESS     @"address"


#endif
