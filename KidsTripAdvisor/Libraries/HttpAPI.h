//
//  HttpAPI.h
//  BarcodeApp
//
//  Created by iOSDev on 3/4/14.
//  Copyright (c) 2014 iOSDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpAPI : NSObject 

+ (void) sendRequest:(BOOL) isPost paramDic:(NSMutableDictionary*)paramDic  url:(NSString *)url completionBlock: (void (^)(BOOL, NSDictionary *, NSError *))completionBlock ;

+ (void) sendPUTRequest:(NSString *) isPost paramDic:(NSMutableDictionary*)paramDic  url:(NSString *)url isFirst:(BOOL)first isLast:(BOOL)last  completionBlock: (void (^)(BOOL, NSDictionary *, NSError *))completionBlock ;



@end
