//
//  KTALoadContentViewController.h
//  KidsTripAdvisor
//
//  Created by Maris on 18/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTALoadContentViewController : UIViewController <NSXMLParserDelegate>

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIProgressView *loadingView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end
