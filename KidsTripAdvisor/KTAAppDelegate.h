//
//  KTAAppDelegate.h
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

//daniel@sharpagency.com.au/123456
// http://dev1.businessprodemo.com/kta/php/admin

#import <UIKit/UIKit.h>
//#import "EncrDcrZip.h"
//EncrDcrZip *EDZ;

#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
int ConnectionName;

NSHTTPURLResponse *httpResponse;

BOOL shouldHideBtn;
@interface KTAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@end
